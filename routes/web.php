<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ManagementController;
use App\Http\Controllers\OrderController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/email/verify', function () {
    return redirect()->to('/auth/profile')->with('auth', 'Harap verifikasi email anda terlebih dahulu');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/auth/profile')->with('authSuccess', 'Selamat!, Email anda telah berhasil diverifikasi.');
})->middleware(['auth', 'signed'])->name('verification.verify');


Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('authSuccess', 'Verifikasi email telah dikirim, harap cek email anda!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

Route::group([
    'prefix' => 'auth'
], function () {

    Route::get('login', [AuthController::class, 'loginPage'])->name('login');
    Route::get('register', [AuthController::class, 'create']);
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('register', [AuthController::class, 'register']);
    Route::get('profile', [AuthController::class, 'profile'])->middleware(['auth', 'customer']);
    Route::put('profile', [AuthController::class, 'profileUpdate'])->middleware(['auth', 'customer']);
    Route::get('profile/edit', [AuthController::class, 'profileEdit'])->middleware(['auth', 'customer']);
});

Route::get('/', [IndexController::class, 'index'])->middleware('customer');
Route::group([
    'prefix' => 'books',
    'middleware' => 'customer'
], function () {
    # guest user
    Route::get('/details/{d}', [BookController::class, 'showDetails'])->name('show');
    Route::get('/{category}', [BookController::class, 'index'])->name('index');
    Route::get('/search', [BookController::class, 'search']);
    Route::get('/{category}/{page}', [BookController::class, 'index']);
});


Route::group(
    [
        'prefix' => 'cart',
        'as' => 'cart.',
        'middleware' => ['auth', 'customer', 'verified']
    ],
    function () {
        Route::post('checkout', [CartController::class, 'checkout'])->name('checkout');
        Route::get('{page}', [CartController::class, 'index']);
        Route::post('add/{id}', [CartController::class, 'add']);
        Route::put('sub/{id}', [CartController::class, 'sub']);
        Route::post('delete/{id}', [CartController::class, 'delete']);
        Route::delete('destroy', [CartController::class, 'destroy']);
    }
);

Route::group(
    [
        'middleware' => ['auth', 'customer', 'verified']
    ],
    function () {
        Route::post('/{id}/book', [CartController::class, 'store']);
        Route::get('/order', [OrderController::class, 'index']);
    }
);

Route::group([
    'prefix' => 'dashboard',
    'as' => 'dashboard.',
    'middleware' => ['auth', 'admin']
], function () {
    Route::get('', [ManagementController::class, 'manageOrders'])->name('index');
    Route::post('books', [BookController::class, 'store']);
    Route::get('/books/new', [BookController::class, 'create']);
    Route::get('/books/{id}', [BookController::class, 'show']);
    Route::get('/books/edit/{id}', [BookController::class, 'edit']);
    Route::post('/books/edit/{id}', [BookController::class, 'update']);
    Route::delete('/books/{id}', [BookController::class, 'delete']);
    Route::get('/orders', [ManagementController::class, 'manageOrders']);
    Route::put('/orders/{id}', [ManagementController::class, 'updateOrder']);

    Route::get('/product-management', [ManagementController::class, 'index']);
});

// Route::redirect('/here', '/there', 301);