<?php

namespace Database\Seeders;

use App\Models\BookCategory;
use App\Models\Publisher;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PublisherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $publishers = [
            [
                'name' => 'M&C!',
                'code' => 'MNC'
            ],
            [
                'name' => 'Elek Media Komputerindo',
                'code' => 'EMK'
            ],
            [
                'name' => 'Erlangga',
                'code' => 'ERL'
            ]
        ];

        foreach ($publishers as $key => $value) {
            $publisher = new Publisher();
            $publisher->name = $value['name'];
            $publisher->code = $value['code'];
            $publisher->save();
        }
    }
}
