<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        // Seed employees
        $users = [
            [
                'name' => 'Admin',
                'email' => 'admin@bookstore.com',
                'role' => 'admin',
                'password' => Hash::make('12345')
            ],
            [
                'name' => 'Ravi',
                'email' => 'ravi@bookstore.com',
                'role' => 'customer',
                'password' => Hash::make('12345')
            ],
            [
                'name' => 'Rudi',
                'email' => 'rudi@bookstore.com',
                'role' => 'customer',
                'password' => Hash::make('12345')
            ]
        ];

        foreach ($users as $index => $value) {
            $user = new User();
            $user->name = $value['name'];
            $user->email = $value['email'];
            $user->role = $value['role'];
            $user->password = $value['password'];
            $user->email_verified_at = Carbon::now();
            $user->save();
            if ($value['role'] == 'customer') {
                $user->customer()->create([
                    'name' => $user->name,
                    'gender' => 'L',
                    'phone' => fake()->phoneNumber(),
                    'address' => fake()->address()
                ]);
            }
        }
        // break;
    }
}
