<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use Faker\Generator as Faker;
use App\Models\BookCategory;
use App\Models\Publisher;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(false);
        $spreadsheet = $reader->load(storage_path("resource/book_seeds.xlsx"));
        $worksheet = $spreadsheet->getActiveSheet();
        $data = new Collection($worksheet->toArray());
        $header = $data->shift();
        $data = $data->forget($header);
        $data = $data->map(function ($item) {
            return [
                'title' => $item[0],
                'price' => $item[2],
                'quantity' => $item[4],
                'isbn' => $item[5],
                'img_path' => $item[6],
                'category' => $item[13],
                'issue_date' => Carbon::make($item[7])->format('Y-m-d'),
                'description' => $item[3],
                'language' => "Indonesia",
                'publisher' => $item[11],
                'author' => $item[12],
                'detail' => [
                    'pages' => $item[1],
                    'weight' => $item[8],
                    'height' => $item[9],
                    'length' => $item[10],
                ]
            ];
        });

        foreach ($data as $key => $value) {
            $author = Author::firstOrCreate([
                'name' => $value['author']
            ]);
            $publisher = Publisher::firstOrCreate([
                'name' => Str::title($value['publisher']),
                'code' => Str::lower($value['publisher'])
            ]);
            $category = BookCategory::updateOrCreate(
                [
                    'value' => Str::lower($value['category'])
                ],
                [
                    'label' => Str::title($value['category']),
                    'value' => Str::lower($value['category'])
                ]
            );
            $book = new Book();
            $book->title = $value['title'];
            $book->price = $value['price'];
            $book->quantity = $value['quantity'];
            $book->isbn = $value['isbn'];
            $book->img_path = null;

            $book->category_id = $category->id;
            $book->issue_date = $value['issue_date'];
            $book->description = $value['description'];
            $book->language = $value['language'];
            $book->publisher_id = $publisher->id;
            $book->author_id = $author->id;
            $book->save();
            $book->detail()->create([
                'pages' => $value['detail']['pages'],
                'height' => $value['detail']['height'],
                'length' => $value['detail']['length'],
                'weight' => $value['detail']['weight']
            ]);

            if (!empty($value['img_path'])) {
                $image = file_get_contents($value['img_path']);
                $path = 'content/img/' . $book->id . '/' . basename($value['img_path']);
                Storage::disk('public')
                    ->put($path, $image);
                $book->img_path = $path;
                $book->update();
            }
        }
    }
}
