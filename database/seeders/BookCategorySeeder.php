<?php

namespace Database\Seeders;

use App\Models\BookCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BookCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'label' => 'Horror',
                'value' => 'horror'
            ],
            [
                'label' => 'Komedi',
                'value' => 'comedy'
            ],
            [
                'label' => 'Novel',
                'value' => 'novel'
            ],
            [
                'label' => 'Petualangan',
                'value' => 'adventure'
            ]
        ];

        foreach ($categories as $key => $value) {
            $category = new BookCategory();
            $category->label = $value['label'];
            $category->value = $value['value'];
            $category->save();
        }
    }
}
