<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = [
            [
                'name' => 'Alex Santoso',
            ],
            [
                'name' => 'Dimas Sasongko',
            ],
            [
                'name' => 'Nolan Grayson',
            ]
        ];

        foreach ($authors as $key => $value) {
            $author = new Author();
            $author->name = $value['name'];
            $author->save();
        }
    }
}
