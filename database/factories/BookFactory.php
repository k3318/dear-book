<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\BookCategory;
use App\Models\Publisher;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Provider as FakerProv;
use Illuminate\Support\Facades\Storage;

use function PHPSTORM_META\type;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book>
 */
class BookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \App\Models\Book::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $pbs = Publisher::pluck('id')->toArray();
        $athrs = Author::pluck('id')->toArray();
        $ctgr = BookCategory::pluck('id')->toArray();
        // fake()->addProvider(new \Faker\Provider\Image(fake()));
        // $imgs = 
        // dd(public_path('content/img'));
        return [
            'title' => fake()->word(),
            'pages' => fake()->numberBetween(1, 300),
            'price' => fake()->biasedNumberBetween(50000, 150000),
            'description' => fake()->realText(),
            'quantity' => 15,
            'code' => fake()->isbn10(),
            'issue_date' => fake()->date(),
            'weight' => fake()->randomFloat(2, 0, 2),
            'language' => fake()->country(),
            'height' => fake()->randomFloat(2, 5, 15),
            'length' => fake()->randomFloat(2, 5, 15),
            'author_id' => fake()->randomElement($pbs),
            'publisher_id' => fake()->randomElement($athrs),
            'category_id' => fake()->randomElement($ctgr)
        ];
    }
}
