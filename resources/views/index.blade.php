@extends('layout.master')
@section('content')
<div class="container-fluid m-0 bg-body bg-primary rounded text-center opening">
    <div class="container-fluid m-0 p-0 rounded">
        <div class="row">
            <div class="col-lg-4 col-md-12 text-lg-left text-sm-center col-sm-12 p-5">
                <h1 class="text-light font-weight-bolder display-4">Dear Book</h1>
                <p class="text-light">Temukan buku - buku favorit anda disini</p>
                <img class="img-fluid" src={{Storage::url('content/img/deer4.png')}} alt="">
            </div>
            <div class="col-lg-8 p-5">
                <div class="row">
                    <div class="col-12">
                        <h1 class="d-none d-xs-block d-md-block d-sm-block d-lg-none my-5">Genre Tersedia</h1>
                    </div>
                    @foreach ($data as $category)
                    <div class="mt-2 col-lg-4 col-md-4 col-sm-6 mt-4">
                        <div class="card border-0 shadow">
                            <img class="card-img-top shadow-sm" src={{ Storage::url($category->book->img_path ?? 'content/img/notfound.jpg') }} alt="Card image cap" style="height: 300px;">
                            <div class="card-body">
                                <h5 class="card-title">{{$category->label}}</h5>
                                <a href={{"/books/".$category->value}} class="stretched-link text-muted">Telusuri</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
@stop