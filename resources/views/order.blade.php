@extends('layout.master')
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
        <li class="breadcrumb-item">Orders</li>
    </ol>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <div class="card">
                <div class="card-header">
                    <h6>Pesanan</h6>
                </div>
                <ul class="list-group list-group-flush">

                </ul>
            </div>
        </div>
        <div class="col-sm-9">
            @if (empty($data->items()))
            <div class="w-100 text-center">
                <img class="img-flui rounded mx-auto d-block " src={{ Storage::url('content/img/no_order.png') }} alt="">
            </div>
            @else
            <div class="container p-3 mb-5 rounded h-100">
                <div class="accordion" id="accordionParent">
                    @foreach ($data->items() as $index => $data)
                    <div class="card">
                        <div class="card-header bg-white position-relative" id="headingOne">
                            <h2 class="mb-0">
                                <div class="d-flex justify-content-between">
                                    <button class="btn btn-link stretched-link" type="button" data-toggle="collapse" data-target={{ '#accordion-' . $index }} aria-expanded="true" aria-controls={{ '#accordion-' . $index }}>
                                        <table class="table text-left">
                                            <tr>
                                                <th>Tanggal Transaksi</th>
                                                <td>{{ $data->created_at->format('d F Y') }}</td>
                                                <td>
                                                    @switch($data->status)
                                                    @case(0)
                                                    <span class="badge badge-pill text-text-dark badge-danger">Dibatalkan</span>
                                                    @break

                                                    @case(1)
                                                    <span class="badge badge-pill text-text-dark badge-warning">Belum
                                                        di Proses</span>
                                                    @break

                                                    @case(2)
                                                    <span class="badge badge-pill text-text-dark badge-info">Sedang
                                                        di Proses</span>
                                                    @break

                                                    @case(3)
                                                    <span class="badge badge-pill text-text-dark badge-info">Sedang
                                                        dalam Perjalanan</span>
                                                    @break

                                                    @case(4)
                                                    <span class="badge badge-pill text-text-dark badge-primary">Telah
                                                        Tiba di Tujuan</span>
                                                    @break

                                                    @case(5)
                                                    <span class="badge badge-pill text-text-dark badge-success">Selesai</span>
                                                    @break
                                                    @endswitch
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Kode Invoice
                                                </th>
                                                <td>
                                                    {{ $data->invoice ?? '-' }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Total Harga
                                                </th>
                                                <td>
                                                    Rp.@convert($data->total_price)
                                                </td>
                                            </tr>
                                        </table>
                                    </button>
                                </div>
                            </h2>
                        </div>

                        <div id={{ 'accordion-' . $index }} class="collapse" aria-labelledby="headingOne" data-parent="#accordionParent">
                            <div class="card-body bg-light">
                                <ul class="list-unstyled">
                                    @foreach ($data['details'] as $details)
                                    <div class="container rounded my-3 p-5 position-relative">
                                        <li class="media">
                                            <img src={{ Storage::url($details['book']['img_path'] ?? 'content/img/notfound.jpg') }} class="align-self-center orders-preview" alt="...">
                                            <div class="media-body mx-5">
                                                <h6 class="text-muted">
                                                    {{ $details['book']['author']['name'] }}
                                                </h6>
                                                <h3 class="mt-0 mb-1"><a class="stretched-link text-dark" href={{ '/books/details/'.$details['book']['id']}}>{{ $details['book']['title'] }}</a></h3>
                                                <table class="table mt-3 bg-white">
                                                    <tr>
                                                        <th>Tanggal Terbit</th>
                                                        <td>{{ $details['book']['issue_date'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Bahasa</th>
                                                        <td>{{ $details['book']['language'] }}</td>
                                                        <th>Panjang</th>
                                                        <td>{{ $details['book']['detail']['height'] }} cm
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>ISBN</th>
                                                        <td>{{ $details['book']['isbn'] }}</td>
                                                        <th>Berat</th>
                                                        <td>{{ $details['book']['detail']['weight'] }} kg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Harga</th>
                                                        <td>Rp.@convert($details['book']['price'])</td>
                                                        <th>Jumlah</th>
                                                        <td>{{$details['quantity']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="999"><hr></td>
                                                        <td><strong>+</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2">Total Harga</th>
                                                        <td>Rp.@convert($details['book']['price'] * $details['quantity'])</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </li>
                                    </div>
                                    @endforeach
                                    <hr>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
</div>
</div>
@stop