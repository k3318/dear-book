<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href={{ asset("css/bootstrap.css") }} />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/solid.css" integrity="sha384-Tv5i09RULyHKMwX0E8wJUqSOaXlyu3SQxORObAI08iUwIalMmN5L6AvlPX2LMoSE" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/fontawesome.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous" />
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <title>Book Store E-Commerce</title>
</head>

<body>
    <!-- search not in navbar collapse -->
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <div class="d-flex flex-grow-1">
            <a href="/" class="navbar-brand">Admin Page</a>
        </div>
        <button class="navbar-toggler order-0" type="button" data-toggle="collapse" data-target="#navbar7">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse order-last" id="navbar7">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href={{ URL::to('/dashboard/orders')}}>Orders</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href={{ URL::to('/dashboard/product-management')}}>CRUD Products</a>
                </li>
                <li class="nav-item">
                    <form action={{ URL::to('/auth/logout') }} method="POST">
                        @csrf
                        <button type="submit" class="btn btn-light">
                            Logout
                        </button>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    <div class="content-all">
        @yield("content")
    </div>
    <script src={{ asset("js/jquery-3.3.1.js") }}></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src={{ asset("js/bootstrap.js") }}></script>
    <script src={{ asset("js/app.js") }}></script>
    <script>
        prevImg = imgPreview.src
        imgInp.onchange = evt => {
            const [file] = imgInp.files
            if (file) {
                imgPreview.src = URL.createObjectURL(file)
            }
        }

    </script>
</body>

</html>
