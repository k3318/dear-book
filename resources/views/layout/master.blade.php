<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href={{ asset('css/bootstrap.css') }} />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/solid.css" integrity="sha384-Tv5i09RULyHKMwX0E8wJUqSOaXlyu3SQxORObAI08iUwIalMmN5L6AvlPX2LMoSE" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/fontawesome.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous" />
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <title>Book Store E-Commerce</title>
</head>

<body>
    <!-- search not in navbar collapse -->
    <nav class="navbar navbar-expand-md  navbar-light bg-light border sticky-top mt-0" style="top: 0;">
        <div class="d-flex flex-grow-1 col-lg-9 col-xl-10 col-md-8 col-sm-12 col-xs-12">
            <a class="navbar-brand" href="/">
                <img src={{ Storage::url('content/img/logo2.png') }} width="100" alt="">
            </a>
            <form class="mr-2 my-auto w-100 d-inline-block order-1" action={{ URL::to('/books/all') }}>
                <div class="input-group">
                    <input type="text" class="form-control border border-right-0" placeholder="Search..." id="search_button" name="search_button">
                    <span class="input-group-append">
                        <button class="btn btn-dark border border-left-0" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
        </div>
        <div class="col-12 d-block d-md-none d-lg-none d-xl-none">
            <ul class="navbar-nav d-flex flex-row justify-content-around ">
                <li class="mx-2 nav-item">
                    <a class="text-dark nav-link" href={{ URL::to('/books/all') }}>
                        <i class="fa fa-book"></i>
                        Semua Buku
                    </a>
                </li>
                <li class="mx-2 nav-item">
                    <a class="text-dark nav-link" href={{ URL::to('/cart/1') }}>
                        <i class="fa fa-shopping-cart"></i>
                        <span class="">Keranjang</span>
                    </a>
                </li>
                <li class="mx-2 nav-item">
                    <a class="text-dark nav-link" href={{ URL::to('/order') }}>
                        <i class="fa fa-shipping-fast"></i>
                        <span class="">Pesanan</span>
                    </a>
                </li>
                <li class="mx-2 nav-item">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href={{ URL::to('/auth/profile') }} class="btn btn-secondary">
                            <img src={{ Storage::url(Session::get('img_path')) }} class="profile-image" width="20">
                            Profil
                        </a>
                        <form action={{ URL::to('/auth/logout') }} method="POST">
                            @csrf
                            <button type="submit" class="btn btn-outline-secondary">
                                <i class="fas fa-power-off mr-2"></i>
                                Logout
                            </button>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        <div class="navbar-collapse collapse flex-shrink-1 flex-grow-0 order-last" id="navbar7">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="text-dark nav-link" href={{ URL::to('/books/all') }}>
                        <i class="fa fa-book"></i>
                        Semua Buku
                    </a>
                </li>
                <li class="nav-item">
                    <a class="text-dark nav-link" href={{ URL::to('/order') }}>
                    </a>
                </li>
                <li class="nav-item">
                    @if (Auth::guest())
                    <a class="btn btn-dark" href={{ URL::to('/auth/login') }}>Log In</a>
                    @else
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle mx-2" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src={{ Storage::url(Session::get('img_path')) }} width="40" height="40" class="rounded-circle">
                    </a>
                    <div class="dropdown-menu border-0 bg-transparent dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <div class="d-flex justify-content-center">
                            <div class="card card-ex rounded">
                                <div class="top-container position-relative">
                                    <img src={{ Storage::url(Session::get('img_path')) }} class="img-fluid profile-image" width="70">
                                    <a class="stretched-link ellipsis ml-2" href={{ URL::to('/auth/profile') }}>
                                        <h5 class="name ellipsis">{{ Auth::user()->customer->name }}</h5>
                                        <p class="text-muted ellipsis">{{Auth::user()->email}}</p>
                                    </a>
                                </div>
                                <div class="wishlist-border pt-2">
                                    <a class="text-dark nav-link" href={{ URL::to('/cart/1') }}>
                                        <i class="fa fa-shopping-cart"></i>
                                        <span class="">Keranjang</span>
                                    </a>
                                </div>
                                <a class="text-dark nav-link" href={{ URL::to('/order') }}>
                                    <i class="fa fa-shipping-fast"></i>
                                    <span class="">Pesanan</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <div class="fashion-studio-border pt-2">
                                    <form action={{ URL::to('/auth/logout') }} method="POST">
                                        @csrf
                                        <button type="submit" class="dropdown-item btn btn-outline-secondary">
                                            <i class="fas fa-power-off mr-2"></i>
                                            Logout
                                        </button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </li>
                @endif
                </li>
            </ul>
        </div>
    </nav>

    <div class="content-all">
        @yield('content')
    </div>
    <!-- End of .container -->
    <script src={{ asset('js/app.js') }}></script>
    <script src={{ asset('js/jquery-3.3.1.js') }}></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src={{ asset('js/bootstrap.js') }}></script>

</body>

</html>
