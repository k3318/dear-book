@extends('layout.master')
@section('content')
@if (\Session::has('success'))
<div class="alert alert-success alert-dismissible fade show mx-5 mt-2" role="alert">
    <strong>Berhasil!.</strong> Profil anda telah berhasil diubah.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@elseif (\Session::has('warning'))
<div class="alert alert-warning alert-dismissible fade show mx-5 mt-2" role="alert">
    <strong>Silahkan, tambahkan alamat anda terlebih dahulu.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="container p-5">
    <div class="row">
        <div class="col-lg-4 mb-5">
            <img src={{Storage::url(Session::get('img_path'))}} class="img-fluid border rounded" alt="Responsive image" id="imgPreview">
        </div>
        <div class="col-lg-8">
            <form action={{URL::to('/auth/profile')}} method="POST">
                @csrf
                @method('PUT')
                <div class="container bg-light border p-3 rounded">
                    <input type="hidden" name="warning" value="{{Session::get('warning') ?? 'NO'}}">
                    <div class="form-group">
                        <label for="inputAddress">Nama</label>
                        <input name="name" type="text" class="form-control" value="{{ $data->customer->name }}" required>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Email</label>
                        <input name="email" type="email" class="form-control" value="{{ $data->email }}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Nomor Telepon</label>
                        <input name="phone" type="text" class="form-control" value="{{ $data->customer->phone }}" required>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Alamat</label>
                        <textarea name="address" class="form-control" rows="5" required>{{ $data->customer->address }}</textarea>
                     
                    </div>
                    <div class="form-group d-flex justify-content-end">
                        <a href={{URL::previous()}} class="btn btn-secondary mx-1">Kembali</a>
                        <button type="submit" class="btn btn-primary mx-1">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop