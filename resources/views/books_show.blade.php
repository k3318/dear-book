@extends('layout.master')
@section('content')
    @if (\Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show mx-5 mt-2" role="alert">
            <strong>Berhasil ditambahkan ke keranjang, silahkan cek keranjang belanjaan anda.</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="container p-5">
        <div class="row">
            <div class="col-sm-4">
                <img class="img-thumbnail shadow-lg"
                    src={{ $data->img_path ? Storage::url($data->img_path) : Storage::url('content/img/notfound.jpg') }}
                    class="img-fluid" alt="Responsive image" id="imgPreview">
            </div>
            <div class="col-sm-8">
                <div class="container p-3 rounded">
                    <div class="d-flex mb-3 justify-content-between">
                        <h5 class="text-muted">{{ $data->author->name }}</h5>
                        <form method="POST" action={{ URL::to('/' . $data->id . '/book') }}>
                            @csrf
                            @if ($data->quantity > 0)
                                <button type="submit" class="btn btn-outline-dark"><i class="fa fa-shopping-cart"></i>
                                    Tambahkan</button>
                            @else
                                <button type="submit" class="btn btn-secondary" disabled><i class="fa fa-shopping-cart"></i>
                                    Tambahkan</button>
                            @endif
                        </form>
                    </div>
                    <h1>{{ $data->title }}</h1>
                    <label for="inputEmail4"><strong>ISBN</strong> {{ $data->isbn }}</label>
                    <hr>
                    <div class="form-group">
                        <label for="inputPassword4"> <strong>Sinopsis</strong></label>
                        <div id="summary">
                            <p class="collapse" id="collapseSummary">{!! nl2br(e($data->description)) !!}</p>
                            <a class="collapsed" data-toggle="collapse" href="#collapseSummary" aria-expanded="false"
                                aria-controls="collapseSummary"></a>
                        </div>
                        <a class="collapsed" data-toggle="collapse" href="#collapseSummary" aria-expanded="false"
                            aria-controls="collapseSummary"></a>
                    </div>
                    <div class="form-group">
                        <table class="table">
                            <tr>
                                <th>Jumlah Stok</th>
                                <td colspan="999">{!! $data->quantity != 0 ? $data->quantity : '<span class="badge badge-danger">Stok Habis</span>' !!}</td>
                            </tr>
                            <tr>
                                <th>Penerbit</th>
                                <td colspan="999">{{ $data->publisher->name ?? '-' }}</td>
                            </tr>
                            <tr>
                                <th>Bahasa</th>
                                <td>{{ $data->language ?? '-' }}</td>
                                <th>Kategori</th>
                                <td>{{ $data->category->label ?? '-' }}</td>
                            </tr>
                            <tr>
                                <td> </td>
                            </tr>
                            <tr>
                                <th>Berat</th>
                                <td>{{ $data->detail->weight ? $data->detail->weight . ' kg' : '-' }}</td>
                                <th>Panjang</th>
                                <td>{{ $data->detail->width ? $data->detail->width . ' cm' : '-' }}</td>
                            </tr>
                            <tr>
                                <th>Lebar</th>
                                <td>{{ $data->detail->length ? $data->detail->length . ' cm' : '-' }}</td>
                                <th>Jumlah Halaman</th>
                                <td>{{ $data->detail->pages ?? '-' }}</td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@stop
