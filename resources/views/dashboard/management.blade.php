@extends('layout.master_dashboard')
@section('content')
    <div class="container-fluid mt-5 p-5">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="text-dark">Book Management</h3>
                <hr>
            </div>
            <div class="col-sm-12">
                <div class="container-fluid bg-light">
                    <nav aria-label="Page navigation example">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group mt-4 mb-2" role="group" aria-label="Basic example">
                                    <a href={{ URL::to('/dashboard/books/new') }} class="btn btn-primary">Create New</a>
                                </div>
                                <div class="d-flex flex-grow-1">
                                    <form class="mr-2 my-auto w-100 d-inline-block order-1" action={{ URL::to('#') }}>
                                        <div class="input-group">
                                            <input type="text" class="form-control border border-right-0"
                                                placeholder="Search..." id="search_button" name="search_button">
                                            <span class="input-group-append">
                                                <button class="btn btn-dark border border-left-0" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                Ditemukan {{ $books['total'] }} item.
                            </div>
                            <div class="col-sm-12">
                                <div class="table-responsive-sm">
                                    <table class="table">
                                        <table class="table table-light">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Book</th>
                                                    <th scope="col">Author Name</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($books['data'] as $index => $book)
                                                    <tr>
                                                        <th scope="row">{{ $index + 1 }}</th>
                                                        <td>{{ $book->title }}</td>
                                                        <td>{{ $book->author_name }}</td>
                                                        <td>Rp.@convert($book->price)</td>
                                                        <td>
                                                            <div class="btn-group" role="group"
                                                                aria-label="Basic example">
                                                                <a href={{ URL::to('/dashboard/books/' . $book->id . '?page=' . $currentPage) }}
                                                                    class="open-Modal btn btn-success"><i
                                                                        class="fas fa-eye"></i></a>
                                                                <a href={{ URL::to('/dashboard/books/edit/' . $book->id . '?page=' . $currentPage) }}
                                                                    class="open-Modal btn btn-primary"><i
                                                                        class="fas fa-edit"></i></a>

                                                                <!-- Button trigger modal -->
                                                                <button type="button" class="open-Modal btn btn-danger"
                                                                    data-toggle="modal" data-target="#exampleModalCenter"><i
                                                                        class="fas fa-trash"></i></button>


                                                                <!-- Modal -->
                                                                <div class="modal fade" id="exampleModalCenter"
                                                                    tabindex="-1" role="dialog"
                                                                    aria-labelledby="exampleModalCenterTitle"
                                                                    aria-hidden="true">
                                                                    <div class="modal-dialog modal-dialog-centered"
                                                                        role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title"
                                                                                    id="exampleModalLongTitle">Apakah anda
                                                                                    yakin?</h5>
                                                                                <button type="button" class="close"
                                                                                    data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>Menghapus buku ini akan menghapus juga
                                                                                    buku yang sudah <b>dipesan</b>. Dan
                                                                                    semua buku ini yang ada di
                                                                                    <b>keranjang</b> pelanggan akan <b
                                                                                        class="text-danger">dihapus</b>.
                                                                                </p>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button"
                                                                                    class="btn btn-secondary"
                                                                                    data-dismiss="modal">Kembali</button>
                                                                                <form
                                                                                    action={{ URL::to('/dashboard/books/' . $book->id) }}
                                                                                    method="POST">
                                                                                    @csrf
                                                                                    @method('DELETE')
                                                                                    <button type="submit"
                                                                                        class="btn btn-danger">Hapus</button>
                                                                                </form>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center">
                                        @if (!empty($books['data']))
                                            @if ($last <= 10)
                                                <li class="page-item disabled">
                                                    <a class="page-link" href="#" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </li>
                                            @else
                                                <li class="page-item">
                                                    <a class="page-link"
                                                        href={{ URL::to('/dashboard/product-management?page=' . ($last - 10)) }}
                                                        aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </li>
                                            @endif
                                            @if ($books['last_page'] >= 10)
                                                @for ($i = $last - 9; $i <= $last; $i++)
                                                    @if ($currentPage == $i)
                                                        <li class="page-item active">
                                                            <a class="page-link" href="#">{{ $i }}</a>
                                                        </li>
                                                    @else
                                                        <li class="page-item">
                                                            <a class="page-link"
                                                                href={{ URL::to($search ? '/dashboard/product-management?page=' . $i . '&search_button=' . $search : '/dashboard/product-management?page=' . $i) }}>{{ $i }}</a>
                                                        </li>
                                                    @endif
                                                @endfor
                                            @else
                                                @for ($i = 1; $i <= $last; $i++)
                                                    @if ($currentPage == $i)
                                                        <li class="page-item active">
                                                            <a class="page-link" href="#">{{ $i }}</a>
                                                        </li>
                                                    @else
                                                        <li class="page-item">
                                                            <a class="page-link"
                                                                href={{ URL::to($search ? '/dashboard/product-management?page=' . $i . '&search_button=' . $search : '/dashboard/product-management?page=' . $i) }}>{{ $i }}</a>
                                                        </li>
                                                    @endif
                                                @endfor
                                            @endif
                                            @if ($last >= $books['last_page'])
                                                <li class="page-item disabled">
                                                    <a class="page-link" href="#" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                    </a>
                                                </li>
                                            @else
                                                <li class="page-item">
                                                    <a class="page-link" href={{ URL::to('/dashboard/product-management?page=' . ($last + 1)) }}
                                                        aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endif
                                </nav>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@stop
