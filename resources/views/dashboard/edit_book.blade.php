@extends('layout.master_dashboard')
@section('content')
@if(\Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Berhasil!</strong>, buku telah berhasil diubah
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="container p-5">
    <div class="row">
        <div class="col-lg-4">
            <img src={{$data->img_path ? Storage::url($data->img_path) : Storage::url('content/img/notfound.jpg')}} class="img-fluid" alt="Responsive image" id="imgPreview">
        </div>
        <div class="col-lg-8">
            <form action={{URL::to("dashboard/books/edit/".$data->id)}} method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <div class="custom-file">
                        <input name="file" type="file" accept="image/png, image/gif, image/jpeg" class="custom-file-input" id="imgInp">
                        <label class="custom-file-label" for="customFile">Pilih Gambar Sampul</label>
                    </div>
                </div>
                <div class="container bg-light border p-3 rounded">
                    <div class="form-group">
                        <label for="inputAddress">Judul</label>
                        <input name="title" type="text" class="form-control" id="title" value="{{$data->title}}" required>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Nama Penulis</label>
                            <div class='input-group mb-3'>
                                <input aria-describedby='button-addon2' aria-label='Country' class='form-control autocomplete' id='author_names' name='author' placeholder='' type='text' value="{{$data->author->name}}">
                                <select class="custom-select" id="authorSelect" hidden>
                                    @foreach ($authors as $author)
                                    <option value="{{$author['label']}}">{{$author['label']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Nama Penerbit</label>
                            <div class='input-group mb-3'>
                                <input aria-describedby='button-addon2' aria-label='Country' class='form-control autocomplete' id='publisher_names' name='publisher' placeholder='' type='text' value="{{$data->publisher->name}}">
                                <select class="custom-select" id="publisherSelect" hidden>
                                    @foreach ($publishers as $publisher)
                                    <option value="{{$publisher['label']}}">{{$publisher['label']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 ">
                            <label for="inputPassword4">Bahasa</label>
                            <input name="language" type="text" class="form-control" id="language" value={{$data->language}}>
                        </div>
                        <div class="form-group col-md-6 ">
                            <label for="inputPassword4">Genre</label>
                            <select name="category_id" class="custom-select" id="category">
                                @foreach ($categories as $category)
                                <option value={{$category['value']}} {{$category['selected'] ? 'selected' : ''}}>{{$category['label']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 " required>
                            <label for="inputPassword4">Harga</label>
                            <input name="price" type="number" class="form-control" id="price" value={{$data->price}}>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="inputPassword4">Stok Tersedia</label>
                            <input name="quantity" type="number" class="form-control" id="quantity" value={{$data->quantity}}>
                        </div>
                        <div class="form-group col-md-3 ">
                            <label for="inputPassword4">Jumlah Halaman</label>
                            <input name="pages" type="number" class="form-control" id="quantity" value={{$data->detail->pages}}>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4">Sinopsis</label>
                        <textarea name="description" class="form-control" id="description" rows="5">{{$data->description}}</textarea>
                    </div>

                    <div class="form-group modal-footer">
                        <a href={{ URL::to('dashboard/product-management?page='.$currentPage) }} class="btn btn-secondary" data-dismiss="modal">Kembali</a>
                        <button type="submit" class="btn btn-primary">Simpan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
