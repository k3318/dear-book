@extends('layout.master_dashboard')
@section('content')
<div class="container p-5">
    <div class="row">
        <div class="col-lg-4">
            <img src={{Storage::url('content/img/notfound.jpg')}} class="img-fluid" alt="Responsive image" id="imgPreview">
        </div>
        <div class="col-lg-8">
            <form action={{ URL::to('/dashboard/books') }} method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlInput1">Book Cover</label>
                    <div class="custom-file">
                        <input name="file" type="file" accept="image/png, image/gif, image/jpeg" class="custom-file-input" id="imgInp">
                        <label class="custom-file-label" for="customFile">Choose Image</label>
                    </div>
                </div>
                <div class="container bg-light border p-3 rounded">
                    <div class="form-group">
                        <label for="inputAddress">Judul</label>
                        <input name="title" type="text" class="form-control" id="title" value="">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">ISBN</label>
                        <input name="isbn" type="text" class="form-control" id="title" value="">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Tanggal Terbit</label>
                        <input name="issue_date" type="date" class="form-control" id="title" value="">
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Penulis</label>
                            <div class='input-group mb-3'>
                                <input aria-describedby='button-addon2' aria-label='Country' class='form-control autocomplete' id='author_names' name='author' placeholder='' type='text'>
                                <select class="custom-select" id="authorSelect" hidden>
                                    @foreach ($authors as $author)
                                    <option value="{{$author['label']}}">{{$author['label']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Penerbit</label>
                            <div class='input-group mb-3'>
                                <input aria-describedby='button-addon2' aria-label='Country' class='form-control autocomplete' id='publisher_names' name='publisher' placeholder='' type='text'>
                                <select class="custom-select" id="publisherSelect" hidden>
                                    @foreach ($publishers as $publisher)
                                    <option value="{{$publisher['label']}}">{{$publisher['label']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 ">
                            <label for="inputPassword4">Bahasa</label>
                            <input name="language" type="text" class="form-control" id="language" value="">
                        </div>
                        <div class="form-group col-md-6 ">
                            <label for="inputPassword4">Genre</label>
                            <select name="category" class="custom-select" id="category">
                                @foreach ($categories as $category)
                                <option value={{$category['value']}}>{{$category['label']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 ">
                            <label for="inputPassword4">Harga</label>
                            <input name="price" type="text" class="form-control" id="price" value="">
                        </div>
                        <div class="form-group col-md-6 ">
                            <label for="inputPassword4">Jumlah Stok</label>
                            <input name="quantity" type="number" class="form-control" id="quantity" value="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 ">
                            <label for="inputPassword4">Jumlah Halaman</label>
                            <input name="pages" type="text" class="form-control" id="price" value="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6 ">
                            <label for="inputPassword4">Lebar Buku</label>
                            <input name="length" type="number" step="0.01" class="form-control" placeholder="Sentimeter">
                        </div>
                        <div class="form-group col-md-6 ">
                            <label for="inputPassword4">Berat Buku</label>
                            <input name="weight" type="number" step="0.01" class="form-control" placeholder="Kilogram">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword4">Sinopsis</label>
                        <textarea name="description" class="form-control" id="description" rows="5" style="white-space: pre-wrap;"></textarea>
                    </div>

                    <div class="form-group modal-footer">
                        <a href={{ URL::to('dashboard/product-management') }} class="btn btn-secondary" data-dismiss="modal">Back</a>
                        <button type="submit" href="" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
