@extends('layout.master_login')
@section('content')
<div class="container-fluid h-100 overflow-hidden bg-light">
    <div class="jumbotron w-100 h-100 bg-transparent">
        <div class="row d-flex bg-info h-100 p-5 border rounded shadow-lg">
            <div class="col-md">
                <h1 class="display-4">Welcome<p>Admin.</p></h1>
                <p class="lead ">please login to continue.</p>
            </div>
            <div class="col-md-8">
                <form class="col-lg-8">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        <small id="emailHelp" class="form-text text-warning">{{$error_msg}}</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn btn-outline-dark">Login</button>

                </form>
            </div>
        </div>
    </div>
</div>
@stop