@extends('layout.master_dashboard')
@section('content')
<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-sm-12">
            <div class="container-fluid bg-light p-4">
                <nav aria-label="Page navigation example">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 class="text-dark">Order Management</h3>
                            <hr>
                        </div>
                        <div class="col-sm-12">
                            <div class="d-flex flex-grow-1">
                                <form class="mr-2 my-auto w-100 d-inline-block order-1" action={{ URL::to('/dashboard') }}>
                                    <div class="input-group">
                                        <input type="text" class="form-control border border-right-0" placeholder="Search..." id="search_button" name="search_button">
                                        <span class="input-group-append">
                                            <button class="btn btn-dark border border-left-0" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    @if (!empty($data['data']))
                                        @if ($last <= 10)
                                            <li class="page-item disabled">
                                                <a class="page-link" href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                    href={{ URL::to('/dashboard'. '?search_button=' . ($last - 10)) }}
                                                    aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                        @endif
                                        @if ($data['last_page'] >= 10)
                                            @for ($i = $last - 9; $i <= $last; $i++)
                                                @if ($currentPage == $i)
                                                    <li class="page-item active">
                                                        <a class="page-link" href="#">{{ $i }}</a>
                                                    </li>
                                                @else
                                                    <li class="page-item">
                                                        <a class="page-link"
                                                            href={{ URL::to($search ? '/dashboard'. '?page=' . $i . '&search_button=' . $search : '/dashboard'. '?page=' . $i) }}>{{ $i }}</a>
                                                    </li>
                                                @endif
                                            @endfor
                                        @else
                                            @for ($i = 1; $i <= $last; $i++)
                                                @if ($currentPage == $i)
                                                    <li class="page-item active">
                                                        <a class="page-link" href="#">{{ $i }}</a>
                                                    </li>
                                                @else
                                                    <li class="page-item">
                                                        <a class="page-link"
                                                            href={{ URL::to($search ? '/dashboard'. '?page=' . $i . '&search_button=' . $search : '/dashboard'. '?page=' . $i) }}>{{ $i }}</a>
                                                    </li>
                                                @endif
                                            @endfor
                                        @endif
                                        @if ($last >= $data['last_page'])
                                            <li class="page-item disabled">
                                                <a class="page-link" href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                    href={{ URL::to('/dashboard' . ($last + 1)) }}
                                                    aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        @endif
                                    @endif
                            </nav>
                        </div>
                        <div class="col-sm-12">
                            <div class="table-responsive-sm">
                                <table class="table">
                                    <table class="table table-light">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Nama Pelanggan</th>
                                                <th scope="col">Pesanan</th>
                                                <th scope="col">Alamat</th>
                                                <th scope="col">Jumlah</th>
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['data'] as $index => $data)
                                            <input type="hidden" value={{ $total = 0 }}>
                                            <tr>
                                                <th scope="row">{{$index + 1}}</th>
                                                <td><b>{{$data['customer']['name']}}</b></td>
                                                <td>
                                                    <ul class="list-unstyled">
                                                        @foreach ($data['details'] as $detail)
                                                        <li class="media mt-3">
                                                            <img src={{Storage::url($detail['book']['img_path']  ?? 'content/img/notfound.jpg')}} class="orders-preview" alt="Responsive image" id="imgPreview">
                                                            <div class="media-body mx-2">
                                                                <h5 class="mt-0 mb-1">{{$detail['book']['title']}}</h5>
                                                                <h6 class="card-subtitle mb-2 text-muted">{{$detail['book']['author']['name']}}</h6>
                                                                <ul>
                                                                    <li><b>Qty</b> {{$detail['quantity']}}</li>
                                                                    <li><b>Harga</b> Rp.@convert($detail['book']['price'])</li>
                                                                    <li><b>Total Harga</b> Rp.@convert(($detail['quantity'] * $detail['book']['price']))</li>
                                                                    <input type="hidden" name="" value={{$total = $total + ($detail['quantity'] * $detail['book']['price'])}}>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>{{$data['customer']['address']}}</td>
                                                <td>@convert($total)</td>
                                                <td>
                                                    <form action={{URL::to('/dashboard/orders/'.$data['id']) }} method="POST">
                                                        @csrf
                                                        @method('PUT')
                                                        <select onchange="this.form.submit()" name="status" class="custom-select" id="category">
                                                            @foreach ($statuses as $status)
                                                            <option value={{$status['value']}} {{$status['value'] == $data['status'] ? 'selected' : ''}}>{{$status['label']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </table>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
@stop