@extends('layout.master_dashboard')
@section('content')
    <div class="container p-5">
        <div class="row">
            <div class="col-lg-4">
                <img src={{ $data->img_path ? Storage::url($data->img_path) : Storage::url('content/img/notfound.jpg') }}
                    class="img-fluid" alt="Responsive image" id="imgPreview">
            </div>
            <div class="col-lg-8">
                <form>
                    <div class="container bg-light border p-3 rounded">
                        <div class="form-group">
                            <label for="inputAddress">Judul</label>
                            <input type="text" class="form-control" id="title" value="{{ $data->title }}" disabled>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Nama Penulis</label>
                                <input type="text" class="form-control" id="author_name"
                                    value="{{ $data->author->name }}" disabled>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Nama Penerbit</label>
                                <input type="text" class="form-control" id="publisher_name"
                                    value="{{ $data->publisher->name }}" disabled>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 ">
                                <label for="inputPassword4">Bahasa</label>
                                <input type="text" class="form-control" id="language" value="{{ $data->language }}"
                                    disabled>
                            </div>
                            <div class="form-group col-md-6 ">
                                <label for="inputPassword4">Genre</label>
                                <select class="custom-select" id="category" disabled>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category['value'] }}"
                                            {{ $category['selected'] ? 'selected' : '' }}>{{ $category['label'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 ">
                                <label for="inputPassword4">Harga</label>
                                @if ($disabled)
                                    <input type="text" class="form-control" id="price" value=Rp.@convert($data->price)
                                        disabled>
                                @else
                                    <input type="number" class="form-control" id="price" value="{{ $data->price }}">
                                @endif
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputPassword4">Stok Tersedia</label>
                                <input type="number" class="form-control" id="quantity" value="{{ $data->quantity }}"
                                    disabled>
                            </div>
                            <div class="form-group col-md-3 ">
                                <label for="inputPassword4">Jumlah Halaman</label>
                                <input name="pages" type="number" class="form-control" id="quantity"
                                    value={{ $data->detail->pages }} disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword4">Sinopsis</label>
                            <textarea class="form-control" id="description" rows="5" disabled>"{{ $data->description }}"</textarea>
                        </div>

                        <div class="form-group modal-footer">
                            <a href={{ URL::to('dashboard/product-management?page=' . $currentPage) }}
                                class="btn btn-secondary" data-dismiss="modal">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
