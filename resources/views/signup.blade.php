@extends('layout.master')

@section('content')
<!-- Section: Design Block -->
@if(\Session::has('success'))
<div class="alert alert-success alert-dismissible fade show mx-5 mt-2" role="alert">
    <strong>Akun anda berhasil dibuat!</strong> silahkan cek verifikasi email yang telah kami kirim
     <!-- Harap verifikasi email anda untuk mulai belanja. -->
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<section class="">
    <!-- Jumbotron -->
    <div class="px-4 py-5 px-md-5 text-center text-lg-start">
        <div class="container">
            <div class="row gx-lg-5 align-items-center">
                <div class="col-lg-6 mb-5">
                    <img src={{Storage::url('content/img/logo.png')}} width="200" height="200">
                    <h5 class="my-5 display-5 fw-bold ls-tight">
                        Buku terbaik hanya ada di <br />
                        <span class="text-warning">dear-book.co.id</span>
                    </h5>
                    <p style="color: hsl(217, 10%, 50.8%)">
                        Gabung dan telusuri buku yang anda sukai
                    </p>
                </div>

                <div class="col-lg-6 mb-5 mb-lg-0 v">
                    <div class="card">
                        <div class="card-body py-5 px-md-5">
                            <form class="vh-50" action={{URL::to('/auth/register')}} method="POST">
                                @csrf
                                <!-- 2 column grid layout with text inputs for the first and last names -->
                                <div class="row">
                                    <div class="col-md-6 mb-4">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example1">First name</label>
                                            <input name="first_name" type="text" id="form3Example1" class="form-control" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <div class="form-outline">
                                            <label class="form-label" for="form3Example2">Last name</label>
                                            <input name="last_name" type="text" id="form3Example2" class="form-control" />
                                        </div>
                                    </div>
                                </div>

                                <!-- Email input -->
                                <div class="form-outline mb-4">
                                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan email">
                                    @if($errors->any())
                                    <small id="emailHelp" class="form-text text-info">{{$errors->first() ?? ''}}</small>
                                    @endif
                                </div>

                                <!-- Password input -->
                                <div class="form-outline mb-4">
                                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required minlength="5">
                                </div>

                                <div class="text-center text-lg-start mt-4 pt-2">
                                    <p class="small fw-bold mt-2 pt-1 mb-0 my-2">Sudah punya akun? <a href={{URL::to('/auth/login') }} class="link-danger"> menuju Login</a></p>
                                    <!-- Submit button -->

                                    <button type="submit" class="btn btn-dark btn-block mb-4">
                                        Daftar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Jumbotron -->
</section>
<!-- Section: Design Block -->
@stop