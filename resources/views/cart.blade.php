@extends('layout.master')
@section('content')
</nav>
@if(\Session::has('success'))
<div class="alert alert-success alert-dismissible fade show mx-5 mt-2" role="alert">
    <strong>Berhasil checkout, pesanan anda akan diproses segera.</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-white">
        <li class="breadcrumb-item">Keranjang</li>
    </ol>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <div class="card border-0">
                    <ul class="list-group list-group-flush">
                        @if(!empty($carts['data']))
                        <li class="list-group-item">
                            <div class="d-flex">
                                <form action="/cart/checkout" method="POST">
                                    @csrf
                                    @foreach($carts['data'] as $key => $cart)
                                    <input type="hidden" value={{$cart->cart_id}} name={{"cart[".$key."]"}} />
                                    @endforeach
                                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModalCenter">Checkout</button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Apakah anda yakin?</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Kembali</button>
                                                    <button type="submit" class="btn btn-primary">Lanjutkan</button>
                                </form>

                            </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    </li>
    @endif
    </ul>
    </div>
    </div>
    <div class="col-sm-9">
        <div class="container p-3 mb-5 rounded h-100">
            @if(empty($carts['data']))
            <div class="w-100 text-center">
                <img class="img-flui rounded mx-auto d-block " src={{Storage::url('content/img/empty_cart.png')}} alt="">
            </div>
            @else
            <div class="row">
                <div class="col-sm-12">
                    <h3>
                        <small class="text-muted">{{empty($carts['data'])? '' : 'Terdapat '. $carts['total']. ' buah item.'}}</small>
                    </h3>
                </div>
                <div class="col-sm-12">

                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            @if(!empty($carts['data']))
                            @if($last <= 10) <li class="page-item disabled">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                                </li>
                                @else
                                <li class="page-item">
                                    <a class="page-link" href={{ URL::to('/books/'.$currentCategory.'/'. ($last - 10)) }} aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                @endif
                                @if($carts['last_page'] >= 10)
                                @for($i = $last - 9; $i <= $last; $i++) @if($currentPage==$i) <li class="page-item active">
                                    <a class="page-link" href={{ URL::to('/cart/'.$i) }}>{{$i}}</a>
                                    </li>
                                    @else
                                    <li class="page-item">
                                        <a class="page-link" href={{ URL::to('/cart/'.$i) }}>{{$i}}</a>
                                    </li>
                                    @endif
                                    @endfor
                                    @else
                                    @for($i = 1; $i <= $last; $i++) @if($currentPage==$i) <li class="page-item active">
                                        <a class="page-link" href={{ URL::to('/cart/'.$i) }}>{{$i}}</a>
                                        </li>
                                        @else
                                        <li class="page-item">
                                            <a class="page-link" href={{ URL::to('/cart/'.$i) }}>{{$i}}</a>
                                        </li>
                                        @endif
                                        @endfor
                                        @endif
                                        @if($last >= $carts['last_page'])
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                        @else
                                        <li class="page-item">
                                            <a class="page-link" href={{ URL::to('/books/'.$currentCategory.'/'. ($last + 1)) }} aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                        @endif
                                        @endif
                        </ul>
                    </nav>
                </div>
                @foreach($carts['data'] as $cart)
                <div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
                    <div class="card border-0 squared" style="border-radius: 0;">
                        <img class="img-book" src={{ Storage::url($cart->img_path ?? 'content/img/notfound.jpg') }} class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title ellipsis"><a class="text-dark stretched-link" href={{ URL::to('/books/details/'. $cart->book_id)}}>{{$cart->title}}</a></h5>
                            <div class="d-flex justify-content-between">
                                <p class="text-muted h6"> Rp. @convert($cart->price * $cart->cart_quantity)</p>
                                <p class="text-muted h6">qty: <small>{{$cart->cart_quantity}}</small></p>
                            </div>
                            <div class="d-flex justify-content-between">
                                <!-- <h5 class="text-muted"> Rp. @convert($cart->price * $cart->cart_quantity)</h5> -->
                                <h5 class="text-muted">
                                    Stok <br>Tersedia: <small>{{$cart->stock}}</small>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end p-3 bg-white">
                        <form method="POST" action={{ URL::to('/cart/delete/'.$cart->cart_id.'?page='.$currentPage) }}>
                            @csrf
                            <button type="submit" class="btn btn-primary mx-1"><i class="fas fa-trash-alt"></i></button>
                            <!-- <a href="#" class="btn btn-primary mx-1"></a> -->
                        </form>
                        <form method="POST" action={{ URL::to('/cart/sub/'.$cart->cart_id.'?page='.$currentPage) }}>
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-outline-primary mx-1"><i class="fas fa-minus"></i></button>
                        </form>
                        @if($cart->stock <= $cart->cart_quantity)
                            <button type="submit" class="btn btn-outline-primary mx-1 disabled"><i class="fas fa-plus"></i></button>
                            @else
                            <form method="POST" action={{ URL::to('/cart/add/'.$cart->cart_id.'?page='.$currentPage) }}>
                                @csrf
                                <button type="submit" class="btn btn-outline-primary mx-1"><i class="fas fa-plus"></i></button>
                            </form>
                            @endif
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <div class="col-sm-12 mt-3">
                <hr>
            </div>
        </div>
    </div>
    </div>

    </div>
    </div>
    @stop