@extends('layout.master')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item">Buku</li>
            <li class="breadcrumb-item active" aria-current="page">{{ $currentCategoryLabel }}</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <div class="card border sticky-top">
                    <div class="card-header bg-white">
                        <h6>Genre</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        @foreach ($categories as $category)
                            @if ($currentCategory == $category['value'])
                                <li class="list-group-item active bg-dark"> <a class="text-light stretched-link"
                                        href="#">{{ $category['label'] }}</a>
                                </li>
                            @else
                                <li class="list-group-item"> <a class="text-dark stretched-link"
                                        href={{ $search ? URL::to('books/' . $category['value'] . '?search_button=' . $search) : URL::to('books/' . $category['value']) }}>{{ $category['label'] }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="container p-3 mb-5 rounded h-100">
                    <div class="row">
                        <div class="col-sm-12">
                            @if ($search)
                                <h3>
                                    Mencari:
                                    <small class="text-muted"> {{ urldecode($search) }}</small>
                                </h3>
                                <p class="lead">
                                    {{ !empty($books['data']) ? 'Ditemukan ' . $books['total'] . ' hasil' : 'Tidak ditemukan apapun.' }}.
                                </p>
                            @endif
                        </div>
                        <div class="col-sm-12">

                            <nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">
                                    @if (!empty($books['data']))
                                        @if ($last <= 10)
                                            <li class="page-item disabled">
                                                <a class="page-link" href="#" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                    href={{ URL::to('/books/' . $currentCategory . '/' . ($last - 10)) }}
                                                    aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                </a>
                                            </li>
                                        @endif
                                        @if ($books['last_page'] >= 10)
                                            @for ($i = $last - 9; $i <= $last; $i++)
                                                @if ($currentPage == $i)
                                                    <li class="page-item active">
                                                        <a class="page-link" href="#">{{ $i }}</a>
                                                    </li>
                                                @else
                                                    <li class="page-item">
                                                        <a class="page-link"
                                                            href={{ URL::to($search ? '/books/' . $currentCategory . '/' . $i . '?search_button=' . $search : '/books/' . $currentCategory . '/' . $i) }}>{{ $i }}</a>
                                                    </li>
                                                @endif
                                            @endfor
                                        @else
                                            @for ($i = 1; $i <= $last; $i++)
                                                @if ($currentPage == $i)
                                                    <li class="page-item active">
                                                        <a class="page-link" href="#">{{ $i }}</a>
                                                    </li>
                                                @else
                                                    <li class="page-item">
                                                        <a class="page-link"
                                                            href={{ URL::to($search ? '/books/' . $currentCategory . '/' . $i . '?search_button=' . $search : '/books/' . $currentCategory . '/' . $i) }}>{{ $i }}</a>
                                                    </li>
                                                @endif
                                            @endfor
                                        @endif
                                        @if ($last >= $books['last_page'])
                                            <li class="page-item disabled">
                                                <a class="page-link" href="#" aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        @else
                                            <li class="page-item">
                                                <a class="page-link"
                                                    href={{ URL::to('/books/' . $currentCategory . '/' . ($last + 1)) }}
                                                    aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                </a>
                                            </li>
                                        @endif
                                    @endif
                            </nav>
                        </div>
                        @if (empty($books['data']))
                            <div class="col-sm-12">
                                <div class="w-100 text-center">
                                    <img class="img-flui rounded mx-auto d-block"
                                        src={{ Storage::url('content/img/content_not_found.png') }} alt="">
                                </div>
                            </div>
                        @else
                            @foreach ($books['data'] as $book)
                                <div class="col-md-4 col-sm-6 col-lg-3">
                                    <div class="card border-0 mt-3">
                                        <div class="book-cover">
                                            <div class="item">
                                                <img class="card-img-top img-book shadow"
                                                    src={{ $book->img_path ? Storage::url($book->img_path) : Storage::url('content/img/notfound.jpg') }}>
                                                @if ($book->quantity <= 0)
                                                    <span class="badge border notify-badge bg-danger text-light">SOLD <br>
                                                        OUT</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="card-title ellipsis">
                                                <a class="stretched-link text-dark font-weight-bold"
                                                    href={{ '/books/details/' . $book->id }}>{{ $book->title }}

                                                </a>

                                            </h6>
                                            <p class="card-subtitle ellipsis text-muted">Oleh {{ $book->author_name ?? 'Unknown' }}
                                            </p>
                                            <!-- <p class="card-text ellipsis">{{ $book->description }}</p> -->
                                            <h5 class="mt-2"> Rp. @convert($book->price)</h5>
                                            <!-- <form method="POST" action={{ URL::to('/' . $book->id . '/book' . '/' . $currentCategory . '/' . $currentPage) }}>
                                                                        @csrf
                                                                        <input type="submit" class="btn btn-outline-primary w-100 mt-1" value="Add to Cart">
                                                                    </form> -->
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                        <div class="col-sm-12 mt-3">
                            <hr>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
@stop
