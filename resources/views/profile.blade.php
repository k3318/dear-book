@extends('layout.master')
@section('content')
@if(\Session::has('auth'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>{{Session::get('auth')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if(\Session::has('authSuccess'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{\Session::get('authSuccess')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="container m-5">
    <div class="row">
        <div class="row user-left-part">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="row ">
                    <div class="col-md-12 col-md-12-sm-12 col-xs-12 user-image text-center">
                        <img src={{Storage::url(Session::get('img_path'))}} style="max-width: 15em;" class="img-fluid rounded-circle">
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <h1>
                            {{$data->customer->name}}
                            <a href={{URL::to('/auth/profile/edit')}} class="btn btn-primary btn-sm"><i class="fas fa-edit"></i>Ubah</a>
                        </h1>
                        @if(!empty(Auth::user()->email_verified_at))
                        <span class="badge badge-success">telah diverifikasi</span>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#profile" role="tab" data-toggle="tab"><i class="fas fa-user-circle"></i> Profil</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12">
                                <table class="table w-100">
                                    <tr>
                                        <th>Email</th>
                                        <td>{{$data->email}}</td>
                                    </tr>
                                    <tr>
                                        <th>Nomor Telepon</th>
                                        <td>{{$data->customer->phone ?? 'Belum Ditambahkan'}}</td>
                                    </tr>
                                    <tr>
                                        <th>Alamat</th>
                                        <td>
                                            {!! nl2br(e($data->customer->address ?? 'Belum Ditambahkan')) !!}
                                        </td>
                                    </tr>
                                    @if(empty(Auth::user()->email_verified_at))
                                    <tr>
                                        <td colspan="999">
                                            <form action={{URL::to("email/verification-notification")}} method="POST">
                                                @csrf
                                                <button class="btn btn-primary">kirim verifikasi email</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@stop
