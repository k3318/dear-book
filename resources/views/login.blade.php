@extends('layout.master')

@section('content')
<section class="overflow-hidden">

    @if(\Session::has('authError'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>{{\Session::get('authError')}}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if(\Session::has('authSuccess'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>{{\Session::get('authSuccess')}}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <!-- Jumbotron -->
    <div class="px-4 py-5 px-md-5 text-center text-lg-start">
        <div class="container">
            <div class="row gx-lg-5 align-items-center">
                <div class="col-lg-6 mb-5">
                    <figure class="figure">
                        <img class="img-fluid" src={{Storage::url('content/img/deer2.jpg')}} width="600">
                        <figcaption class="figure-caption">Belanja sekarang dan temukan buku - buku favorit anda.</figcaption>
                    </figure>
                </div>

                <div class="col-lg-6 mb-5 mb-lg-0">
                    <div class="card">
                        <div class="card-body py-5 px-md-5">
                            <form class="mh-100" action={{ URL::to('/auth/login') }} method="POST">
                                @csrf
                                <!-- Email input -->
                                <div class=" form-outline mb-4">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                    @if($errors->any())
                                    <small id="emailHelp" class="form-text text-warning">{{$errors->first() ?? ''}}</small>
                                    @endif
                                </div>

                                <!-- Password input -->
                                <div class="form-outline mb-4">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="text-center text-lg-start mt-4 pt-2">
                                    <p class="small fw-bold mt-2 pt-1 mb-0 my-2">Belum punya akun? <a href={{URL::to('/auth/register') }} class="link-danger">Ayo daftar !</a></p>
                                    <!-- Submit button -->
                                    <button type="submit" class="btn btn-dark btn-block mb-4">
                                        Login
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Jumbotron -->
</section>
@stop
