<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model
{
    use HasFactory;
    protected $fillable = [
        'label',
        'value',
    ];
    public function books()
    {
        return $this->hasMany(Book::class, 'category_id', 'id');
    }
    public function book()
    {
        return $this->hasOne(Book::class, 'category_id', 'id');
    }
}
