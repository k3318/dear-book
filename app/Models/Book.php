<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    /**
     * Get category of the books.
     */
    public function category()
    {
        return $this->belongsTo(BookCategory::class);
    }

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }
    
    public function detail()
    {
        return $this->hasOne(BookDetail::class);
    }
}
