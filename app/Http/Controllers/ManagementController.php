<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $last = 10;
        $page = $request->page ?? 1;
        while ($page > $last) {
            $last = $last + 10;
        }

        $perPage = $request->perPage ?? 5;

        $search = $request->search_button ?? false;

        // dd($search);
        // dd(urldecode($search));

        $books = DB::table('books')
            ->leftJoin('authors', 'books.author_id', 'authors.id')
            ->select(
                'books.*',
                'book_categories.label as category_name',
                'book_categories.label as category',
                'authors.name as author_name',
                DB::raw('CASE WHEN books.quantity < 1 THEN 0 ELSE 1 END AS availability')
            );

        $b_categories = DB::table('book_categories');

        $books->joinSub($b_categories, 'book_categories', 'books.category_id', 'book_categories.id');

        if ($search) {
            $books->where('books.title', 'like', "%$search%")
                ->orWhere('book_categories.value', 'like', "%$search%")
                ->orWhere('authors.name', 'like', "%$search%");
        }

        $results = $books
            ->orderBy('availability', 'desc')
            ->orderBy('books.created_at')
            ->orderBy('books.title')
            ->paginate($perPage, ['*'], 'page', $page)->toArray();
        // dd($results);
        if ($last > $results['last_page']) {
            $last = $results['last_page'];
        }

        return view('dashboard.management', [
            'books' => $results,
            'search' => urlencode($search),
            'currentPage' => $page,
            'last' => $last
        ]);
    }

    public function updateOrder(Request $request, $id)
    {
        $order = Order::find($id);
        $order->status = $request->status;
        $order->save();

        return redirect()->back();
    }
    public function manageOrders(Request $request)
    {
        $last = 10;
        $page = $request->page ?? 1;
        $perPage = $request->perPage ?? 1;
        while ($page > $last) {
            $last = $last + 10;
        }

        $search = $request->search_button ?? false;

        $orders = Order::with(['details.book.detail', 'details.book.author', 'customer.user']);

        if ($search) {
            $orders->whereHas('details.book', function ($query) use ($search) {
                $query->where('title','like', "%$search%");
            });
            $orders->orWhereHas('details.book.author', function ($query) use ($search) {
                $query->where('authors.name','like', "%$search%");
            });
            $orders->orWhereHas('customer', function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
                $query->orWhere('address', 'like', "%$search%");
            });
            // dd($orders->toSql());
        }

        $results = $orders
            ->paginate($perPage, ['*'], 'page', $page)->toArray();
        if ($last > $results['last_page']) {
            $last = $results['last_page'];
        }

        $statuses = [
            [
                'label' => 'Dibatalkan',
                'value' => 0
            ],
            [
                'label' => 'Belum di Proses',
                'value' => 1
            ],
            [
                'label' => 'Sedang di Proses',
                'value' => 2
            ],
            [
                'label' => 'Sedang dalam Perjalanan',
                'value' => 3
            ],
            [
                'label' => 'Telah Tiba di Tujuan',
                'value' => 4
            ],
            [
                'label' => 'Selesai',
                'value' => 5
            ],
        ];

        return view('dashboard.orders_management', [
            'data' => $results,
            'search' => urlencode($search),
            'currentPage' => $page,
            'last' => $last,
            'statuses' => $statuses
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $books = DB::table('books as b')
            ->leftJoin('authors as a', 'b.author_id', 'a.id')
            ->leftJoin('book_categories as bc', 'b.category_id', 'bc.id')
            ->leftJoin('publishers as p', 'b.publisher_id', 'p.id');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
