<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Cart;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = 1)
    {
        $user = Auth::id();

        $last = 10;
        $page = $page ?? 1;
        while ($page > $last) {
            $last = $last + 10;
        }


        $customer = DB::table('users as u')
            ->join('customers as c', 'u.id', 'c.id')
            ->where('c.user_id', $user)
            ->first();
        // dd($user);

        $carts = DB::table('carts as c')
            ->select('c.*')
            ->where('c.customer_id', $customer->id);

        $items = DB::table('books as b')
            ->select('b.*', 'jc.quantity as cart_quantity', 'jc.id as cart_id', 'a.name as author_name', 'b.quantity as stock', 'b.id as book_id')
            ->leftJoin('authors as a', 'a.id', 'b.author_id')
            ->joinSub($carts, 'jc', 'b.id', 'jc.book_id');

        $results = $items->paginate(6, ['*'], 'page', $page)->toArray();
        // dd($results);
        // dd($results);
        if ($last > $results['last_page']) {
            $last = $results['last_page'];
        }
        return view('cart', [
            'carts' => $results,
            'currentPage' => $page,
            'last' => $last
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function add(Request $request, $id)
    {
        $page = $request->page ?? 1;
        // dd($id);
        $cart = Cart::find($id);
        // dd($cart->quantity);
        $cart->quantity = $cart->quantity + 1;
        $cart->save();

        return redirect('cart/' . $page);
    }

    public function sub(Request $request, $id)
    {
        $page = $request->page ?? 1;

        $cart = Cart::find($id);
        if ($cart->quantity == 1) {
            $cart->delete();
        } else {
            $cart->quantity = $cart->quantity - 1;
            $cart->update();
        }
        return redirect('cart/' . $page);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        // dd($id);
        $user = Auth::id();

        $customer = DB::table('users as u')
            ->join('customers as c', 'u.id', 'c.id')
            ->where('c.user_id', $user)
            ->first();


        $itemAlreadyInCart = DB::table('carts as c')
            ->where('c.customer_id', $customer->id)
            ->where('c.book_id', $id)
            ->first();

        if ($itemAlreadyInCart) {
            $cart = Cart::with('book')->find($itemAlreadyInCart->id);

            if($cart->book->quantity > $cart->quantity){
                $cart->quantity = $cart->quantity + 1;
            }
            $cart->save();
        } else {
            $cart = new Cart();
            $cart->customer_id = $customer->id;
            $cart->book_id = $id;
            $cart->save();
        }

        // dd($cart);

        return redirect()->back()->with(['success' => 'OK']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = Auth::id();

        $customer = DB::table('users as u')
            ->join('customers as c', 'u.id', 'c.id')
            ->where('c.user_id', $user)
            ->first();

        $carts = DB::table('carts as c')
            ->select('c.*')
            ->where('c.customer_id', $customer->id);
        $carts->delete();

        return redirect('cart/1');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $page = $request->page ?? 1;

        $cart = Cart::find($id);
        $cart->delete();
        return redirect('cart/' . $page);
    }

    public function checkout(Request $request)
    {
        $price = Cart::with('book')->whereIn('id', $request->cart)
            ->get()
            ->map(function ($item) {
                return [
                    'price' => $item->quantity * $item->book->price
                ];
            })
            ->sum('price');
        $inv_date = Carbon::now()->format('dmY');
        $customer = Auth::user()->customer;
        $customer_id = $customer->id;
        if (empty($customer->address)) {
            // dd($customer->address);
            return redirect('/auth/profile/edit')->with(['warning' => 'YES']);
        }
        $order = new Order();
        $orderCount = Customer::find($customer_id)->load('orders')->whereHas('orders')->count() ?? 0;
        $order->status = 1;
        $order->invoice = "INV-" . $inv_date . "-" . $customer_id . '-' . ($orderCount + 1);
        $order->customer_id = $customer_id;
        $order->total_price = $price;
        $order->save();
        foreach ($request->cart as $id) {
            $item = Cart::find($id);
            $order_detail = new OrderDetail();
            $order_detail->book_id = $item->book_id;
            $order_detail->quantity = $item->quantity;
            $order_detail->order_id = $order->id;
            $order_detail->save();
            $book = Book::find($item->book_id);
            $book->quantity = $book->quantity - $item->quantity;
            // dd($book->quantity);
            $book->save();
            $item->delete();
        }

        return redirect()->back()->with(['success' => 'OK']);
    }
}
