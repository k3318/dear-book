<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use App\Models\BookCategory;
use App\Models\Publisher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $category = 'all', $page = 1)
    {
        $last = 10;
        $page = $page ?? 1;
        $perPage = $request->perPage ?? 12;
        while ($page > $last) {
            $last = $last + 10;
        }

        $search = $request->search_button ?? false;

        // dd($search);
        // dd(urldecode($search));

        $books = DB::table('books')
            ->leftJoin('authors', 'books.author_id', 'authors.id')
            ->leftJoin('publishers', 'books.publisher_id', 'publishers.id')
            ->select(
                'books.*',
                'book_categories.label as category_name',
                'book_categories.label as category',
                'authors.name as author_name',
                DB::raw('CASE WHEN books.quantity < 1 THEN 0 ELSE 1 END AS availability')
            );
        $categories = DB::table('book_categories')->get()->map(
            function ($item) {
                return [
                    'label' => $item->label,
                    'value' => $item->value
                ];
            }
        );
        $categories = $categories->prepend(['label' => 'Semua', 'value' => 'all']);
        $b_categories = DB::table('book_categories');
        // dd($books);
        if ($category != 'all') {
            $b_categories->where('book_categories.value', $category);
            // dd($category);
        }
        $books->joinSub($b_categories, 'book_categories', 'books.category_id', 'book_categories.id');

        if ($search) {
            $books->where('books.title', 'like', "%$search%")
                ->orWhere('book_categories.label', 'like', "%$search%")
                ->orWhere('authors.name', 'like', "%$search%")
                ->orWhere('publishers.name', 'like', "%$search%");
        }

        $results = $books
            ->orderBy('availability', 'desc')
            ->orderBy('books.created_at')
            ->orderBy('books.title')
            ->paginate($perPage, ['*'], 'page', $page)->toArray();
        // dd($results);
        if ($last > $results['last_page']) {
            $last = $results['last_page'];
        }

        $category = $categories->where('value', $category)->first();

        return view(
            'books',
            [
                'books' => $results,
                'search' => urlencode($search),
                'categories' => $categories,
                'currentCategory' => $category['value'],
                'currentCategoryLabel' => $category['label'],
                'currentPage' => $page,
                'last' => $last
            ]
        );
    }

    public function showDetails(Request $request, $id)
    {
        $books = Book::find($id)
            ->load(['author', 'publisher', 'category', 'detail']);
        // DB::table('books as b')
        //     ->select('b.*', 'p.name as publisher_name', 'a.name as author_name', 'bc.label as category_name', 'bc.value as category_code')
        //     ->leftJoin('authors as a', 'b.author_id', 'a.id')
        //     ->leftJoin('book_categories as bc', 'b.category_id', 'bc.id')
        //     ->leftJoin('publishers as p', 'b.publisher_id', 'p.id')
        //     ->where('b.id', $id)
        //     ->first();

        $categories = DB::table('book_categories')->get()->map(
            function ($item) use ($books) {
                return [
                    'label' => $item->label,
                    'value' => $item->value,
                    'selected' => $books->category_code == $item->value ? true : false
                ];
            }
        );
        // $categories = $categories->prepend(['label' => 'Open this select menu', 'value' => '', 'selected' => true]);
        // dd($books->first());
        return view(
            'books_show',
            [
                'data' => $books
            ]
        );
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = BookCategory::all()
            ->map(
                function ($item) {
                    return [
                        'label' => $item->label,
                        'value' => $item->id
                    ];
                }
            )->toArray();
        $authors = Author::all()
            ->map(
                function ($item) {
                    return [
                        'label' => $item->name,
                        'value' => $item->id
                    ];
                }
            )->toArray();
        $publishers = Publisher::all()
            ->map(
                function ($item) {
                    return [
                        'label' => $item->name,
                        'value' => $item->id
                    ];
                }
            )->toArray();

        // dd($categories);

        return view(
            'dashboard.create_book',
            [
                'categories' => $categories,
                'authors' => $authors,
                'publishers' => $publishers
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $publisher = Publisher::where('name', $request->publisher)
            ->orWhere('code', strtolower($request->publisher))
            ->first();
        $author = Author::where('name', $request->author)
            ->first();

        if (empty($author)) {
            $author = new Author();
            $author->name = $request->author;
            $author->save();
        }

        if (empty($publisher)) {
            $publisher = new Publisher();
            $publisher->name = $request->publisher;
            $publisher->code = strtolower($request->publisher);
            $publisher->save();
        }
        $book = new Book();
        $book->title = $request->title;
        $book->isbn = $request->isbn;
        $book->language = $request->language;
        $book->quantity = $request->quantity;
        $book->price = $request->price;
        $book->author_id = $author->id;
        $book->publisher_id = $publisher->id;
        $book->category_id = $request->category;
        $book->description = $request->description;
        $book->issue_date = $request->issue_date;
        $book->save();
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->store('content/img/' . $book->id, ['disk' => 'public']);
            $book->img_path = $path;
            $book->update();
        }
        $book->detail()->create(
            [
                'pages' => $request->pages,
                'height' => $request->height,
                'weight' => $request->weight,
                'length' => $request->length
            ]
        );

        return redirect()->to("/dashboard/product-management")->with(['success' => 'OK']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $books = Book::find($id)
            ->load(['author', 'publisher', 'category', 'detail']);
        // DB::table('books as b')
        //     ->select('b.*', 'p.name as publisher_name', 'a.name as author_name', 'bc.label as category_name', 'bc.value as category_code')
        //     ->leftJoin('authors as a', 'b.author_id', 'a.id')
        //     ->leftJoin('book_categories as bc', 'b.category_id', 'bc.id')
        //     ->leftJoin('publishers as p', 'b.publisher_id', 'p.id')
        //     ->where('b.id', $id)
        //     ->first();

        $categories = DB::table('book_categories')->get()->map(
            function ($item) use ($books) {
                return [
                    'label' => $item->label,
                    'value' => $item->value,
                    'selected' => $books->category_code == $item->value ? true : false
                ];
            }
        );
        // $categories = $categories->prepend(['label' => 'Open this select menu', 'value' => '', 'selected' => true]);
        // dd($books->first());
        return view(
            'dashboard.show_book',
            [
                'data' => $books,
                'disabled' => true,
                'categories' => $categories,
                'currentPage' => $request->page ?? 1
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $books = Book::find($id)
            ->load(['detail', 'author', 'publisher', 'category']);
        //  DB::table('books as b')
        //     ->select('b.*', 'p.name as publisher_name', 'a.name as author_name', 'bc.label as category_name', 'bc.id as category_code', 'p.id as publisher_code', 'a.id as author_code')
        //     ->leftJoin('authors as a', 'b.author_id', 'a.id')
        //     ->leftJoin('book_categories as bc', 'b.category_id', 'bc.id')
        //     ->leftJoin('publishers as p', 'b.publisher_id', 'p.id')
        //     ->where('b.id', $id)
        // ->first();

        // dd($books->toArray());
        $categories = DB::table('book_categories')->get()->map(
            function ($item) use ($books) {
                return [
                    'label' => $item->label,
                    'value' => $item->id,
                    'selected' => $books->category->id == $item->id ? true : false
                ];
            }
        );
        $publishers = DB::table('publishers')->get()->map(
            function ($item) use ($books) {
                return [
                    'label' => $item->name,
                    'value' => $item->id,
                    'selected' => $books->publisher->id == $item->id ? true : false
                ];
            }
        );
        $authors = DB::table('authors')->get()->map(
            function ($item) use ($books) {
                return [
                    'label' => $item->name,
                    'value' => $item->id,
                    'selected' => $books->author->id == $item->id ? true : false,
                ];
            }
        );
        // $categories = $categories->prepend(['label' => 'Open this select menu', 'value' => '', 'selected' => true]);
        // dd($books->first());

        return view(
            'dashboard.edit_book',
            [
                'data' => $books,
                'disabled' => false,
                'categories' => $categories,
                'authors' => $authors,
                'publishers' => $publishers,
                'currentPage' => $request->page ?? 1
                // 'success' => $request->success ?? false
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $publisher = Publisher::where('name', $request->publisher)
            ->orWhere('code', strtolower($request->publisher))
            ->first();
        $author = Author::where('name', $request->author)
            ->first();

        if (empty($author)) {
            $author = new Author();
            $author->name = $request->author;
            $author->save();
        }

        if (empty($publisher)) {
            $publisher = new Publisher();
            $publisher->name = $request->publisher;
            $publisher->code = strtolower($request->publisher);
            $publisher->save();
        }
        $book = Book::find($id);
        $book->title = $request->title;
        $book->language = $request->language;
        $book->quantity = $request->quantity;
        $book->price = $request->price;
        $book->author_id = $author->id;
        $book->publisher_id = $publisher->id;
        $book->category_id = $request->category_id;
        $book->description = $request->description;
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = $file->store('content/img/' . $id, ['disk' => 'public']);
            $book->img_path = $path;
        }
        $book->save();
        $book->detail()->update(
            [
                'pages' => $request->pages
            ]
        );

        return redirect()->back()->with(['success' => 'OK']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // dd($id);
        $book = Book::find($id);
        if ($book->delete()) {
            return redirect()->back()->with(['message' => 'Book has succesfully deleted!']);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
