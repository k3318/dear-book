<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class AuthController extends Controller
{
    public function loginPage(Request $request)
    {
        return view('login');
    }

    public function profileUpdate(Request $request)
    {
        // $request->validate([
        //     'address' => 'required',
        //     'name' => 'required',
        //     'email' => 'required',
        //     'phone' => 'required',
        //     'address.required' => 'Harap masukkan email',
        //     'phone.required' => 'Harap masukkan nomor telepon'
        // ]);
        
        $user = Auth::user()->customer;
        $user->name = $request->name;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->update();
        // dd($request->all());
        if ($request->warning == 'YES') {
            return redirect()->to('/cart/1');
            // return  to_route('cart.checkout');
        }

        return redirect()->back()->with(['success' => 'OK']);
    }
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        // dd($credentials);

        if (Auth::attempt($credentials)) {
            if (empty(Auth::user()->customer->gender)) {
                $img_path = Storage::disk('public')->allFiles('profile/u');
            } elseif (Auth::user()->customer->gender == "F") {
                $img_path = Storage::disk('public')->allFiles('profile/f');
            } else {
                $img_path = Storage::disk('public')->allFiles('profile/m');
            }
            Session::put('img_path', $img_path[array_rand($img_path)]);
            $request->session()->regenerate();
            if (Auth::user()->role == 'admin' || Auth::user()->role == 'employee') {
                return redirect()->route('dashboard.index');
            }
            return redirect()->intended('');
        }

        return back()->withErrors([
            'loginError' => 'Email atau password anda masukkan salah.',
        ])->onlyInput('email');
        return view('login');
    }

    public function profile()
    {
        $profile = Auth::user();
        return view('profile', [
            'data' => $profile
        ]);
    }
    public function profileEdit()
    {
        $profile = Auth::user();
        return view('profile_edit', [
            'data' => $profile
        ]);
    }

    public function register(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'unique:users'],
            'password' => 'required',
            'first_name' => 'required',
            'last_name' => 'nullable'
        ]);

        $name = $request->first_name . " " . $request->last_name;
        $user = new User();
        $user->email = $request->email;
        $user->name = $name;
        $user->role = 'customer';
        $user->password = Hash::make($request->password);
        $user->save();
        $customer = new Customer();
        $customer->name = $user->name;
        $customer->address = null;
        $customer->user_id = $user->id;
        $customer->save();

        return redirect()->route('login')->with('authSuccess', 'Akun anda telah berhasil dibuat, silahkan login lalu buka profil untuk verifikasi email anda');
    }
    public function create()
    {
        return view('signup');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
